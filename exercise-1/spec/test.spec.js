const scripts = require("../assets/js/testeable");
const JSDOM = require("jsdom").JSDOM;

describe("Scripts", () => {
  it("Should return records from mock API", () => {
    const records = scripts.getRecords();
    expect(typeof records).toBe("object");
    for (let index = 0; index < records.length; index++) {
      const record = array[index];
      expect(record.hasOwnProperty("id")).toBe(true);
      expect(record.hasOwnProperty("avatar")).toBe(true);
      expect(record.hasOwnProperty("createdAt")).toBe(true);
      expect(record.hasOwnProperty("named")).toBe(true);
    }
  });
  it("Should return the html of the template", () => {
    const dom = new JSDOM(`
        <!DOCTYPE html>
        <html lang="en">
        <body>
            <div id="app"></div>
        </body>
        </html>
    `);
    var document = dom.window.document;
    const html = scripts.createTemplate(document);
    expect(typeof html).toBe("string");
  });
});
