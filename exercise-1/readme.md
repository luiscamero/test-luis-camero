# Luis Camero's test

This test was made using [this](https://github.com/AbercrombieAndFitch/cs-code-challenge) as guideline.

## Installation

To install dependencies run

```bash
npm install
```

For jasmine tests, use

```bash
npm run test
```
