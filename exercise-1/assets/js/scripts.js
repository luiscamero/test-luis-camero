let records = [];
async function getRecords() {
  const data = await fetch(
    "https://5dc588200bbd050014fb8ae1.mockapi.io/assessment",
    {
      method: "get",
    }
  )
    .then((response) => response.json())
    .then((data) => {
      console.log(JSON.stringify(data));
      records = data;
      records.map((record) => {
        record.active = false;
        record.createdAt = new Intl.DateTimeFormat("en").format(
          new Date(record.createdAt)
        );
        return record;
      });
      createTemplate();
    });
}
function createTemplate() {
  // compile the template
  const template = Handlebars.compile(`
        <ul class="list-group">
            {{#each users}}
              <li class="user list-group-item">
                <div class="row">
                  <div class="col col-md-1">
                    <img class="img-thumbnail rounded-circle avatar" src="{{avatar}}" />
                  </div>
                  <div class="col col-md-11 d-flex align-items-center flex-row">
                    <span>Name: {{name}} <span id="hidden-data-{{id}}" class="d-none">
                    - ID: {{id}} - Created At: {{createdAt}}
                  </span> </span>
                    
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-5 col-md-3">
                    <button id="inactive-{{id}}" class="btn btn-primary action-button">
                      Show more
                    </button>
                    <button id="active-{{id}}" class="btn btn-danger action-button d-none">
                      Show less
                    </button>
                  </div>
                </div>
              </li>
            {{/each}}
        </ul>
    `);
  // execute the compiled template
  const app = document.getElementById("app");
  app.innerHTML = template({ users: records });

  const clickHandler = (event) => {
    const record = records.find(
      (record) => record.id == event.srcElement.id.split("-")[1]
    );
    record.active = !record.active;
    const activeButton = document.getElementById(`active-${record.id}`);
    const inactiveButton = document.getElementById(`inactive-${record.id}`);
    const hiddenData = document.getElementById(`hidden-data-${record.id}`);
    if (record.active) {
      inactiveButton.classList.add("d-none");
      activeButton.classList.remove("d-none");
      hiddenData.classList.remove("d-none");
    } else {
      inactiveButton.classList.remove("d-none");
      activeButton.classList.add("d-none");
      hiddenData.classList.add("d-none");
    }
  };

  errorHandler = (event) => {
    event.srcElement.src = "./assets/img/avatar.png";
  };

  const buttons = document.getElementsByClassName("action-button");
  Array.from(buttons).forEach((element) => {
    element.addEventListener("click", clickHandler);
  });

  const images = document.getElementsByClassName("avatar");
  Array.from(images).forEach((element) => {
    element.addEventListener("error", errorHandler);
  });
}

getRecords();
