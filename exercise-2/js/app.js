const taskInput = document.getElementById("new-task");
const addButton = document.getElementsByTagName("button")[0];
const incompleteTasksHolder = document.getElementById("incomplete-tasks");
const completedTasksHolder = document.getElementById("completed-tasks");
const storedTasks = JSON.parse(localStorage.getItem("tasks"));

const createNewTaskElement = function (taskString, completed) {
  const listItem = document.createElement("li");
  const checkBox = document.createElement("input");
  const label = document.createElement("label");
  const editInput = document.createElement("input");
  const editButton = document.createElement("button");
  const deleteButton = document.createElement("button");

  checkBox.type = "checkbox";
  editInput.type = "text";
  editButton.innerText = "Edit";
  editButton.className = "edit";
  deleteButton.innerText = "Delete";
  deleteButton.className = "delete";
  label.innerText = taskString;

  if (completed) {
    checkBox.checked = true;
  }

  listItem.appendChild(checkBox);
  listItem.appendChild(label);
  listItem.appendChild(editInput);
  listItem.appendChild(editButton);
  listItem.appendChild(deleteButton);

  return listItem;
};

const addTask = function (itemName, isNew = true, completed = false) {
  const listItemName = isNew ? taskInput.value : itemName;
  if (listItemName.length) {
    listItem = createNewTaskElement(listItemName, completed);
    if (!completed) {
      incompleteTasksHolder.appendChild(listItem);
      bindTaskEvents(listItem, taskCompleted);
    } else {
      completedTasksHolder.appendChild(listItem);
      bindTaskEvents(listItem, taskIncomplete);
    }
    if (isNew) {
      console.log(listItemName);
      addToLocalstorage(listItemName, false);
    }
    taskInput.value = "";
  } else {
    alert("Task name can't be empty");
  }
};

const addToLocalstorage = (name, completed) => {
  const storedTasks = JSON.parse(localStorage.getItem("tasks"));
  if (storedTasks) {
    if (completed) {
      storedTasks.completed.push(name);
    } else {
      storedTasks.incomplete.push(name);
    }
    localStorage.setItem("tasks", JSON.stringify(storedTasks));
  } else {
    const tasks = {
      completed: [],
      incomplete: [],
    };
    if (completed) {
      tasks.completed.push(name);
    } else {
      tasks.incomplete.push(name);
    }
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }
};

const editTask = function () {
  const listItem = this.parentNode;
  const editInput = listItem.querySelectorAll("input[type=text")[0];
  const label = listItem.querySelector("label");
  const button = listItem.getElementsByTagName("button")[0];

  const containsClass = listItem.classList.contains("editMode");
  let error = false;
  if (containsClass) {
    if (editInput.value.length) {
      const isChecked = listItem.querySelectorAll("input[type=checkbox")[0]
        .checked;
      const storedTasks = JSON.parse(localStorage.getItem("tasks"));
      if (storedTasks) {
        if (isChecked) {
          storedTasks.completed = storedTasks.completed.map((task) => {
            if (task == label.innerText) {
              task = editInput.value;
            }
            return task;
          });

          localStorage.setItem(tasks, JSON.stringify(storedTasks));
        } else {
          storedTasks.incomplete = storedTasks.incomplete.map((task) => {
            if (task == label.innerText) {
              task = editInput.value;
            }
            return task;
          });
          localStorage.setItem("tasks", JSON.stringify(storedTasks));
        }
      } else {
        const tasks = {
          completed: [],
          incomplete: [],
        };
        if (completed) {
          tasks.completed.push(editInput.value);
        } else {
          tasks.incomplete.push(editInput.value);
        }
        localStorage.setItem("tasks", JSON.stringify(tasks));
      }

      label.innerText = editInput.value;
      button.innerText = "Edit";
    } else {
      alert("Task name can't be empty");
      error = true;
    }
  } else {
    editInput.value = label.innerText;
    button.innerText = "Save";
  }

  if (!error) {
    listItem.classList.toggle("editMode");
  }
};

const deleteTask = function (el) {
  const listItem = this.parentNode;
  const isChecked = listItem.querySelectorAll("input[type=checkbox")[0].checked;
  const label = listItem.querySelectorAll("label")[0];
  const storedTasks = JSON.parse(localStorage.getItem("tasks"));
  if (storedTasks) {
    if (isChecked) {
      storedTasks.completed = storedTasks.completed.filter(
        (task) => task != label.innerText
      );

      localStorage.setItem(tasks, JSON.stringify(storedTasks));
    } else {
      storedTasks.incomplete = storedTasks.incomplete.filter(
        (task) => task != label.innerText
      );
      localStorage.setItem("tasks", JSON.stringify(storedTasks));
    }
  }

  const ul = listItem.parentNode;
  ul.removeChild(listItem);
};

const taskCompleted = function (el) {
  const listItem = this.parentNode;

  const label = listItem.querySelectorAll("label")[0];
  const storedTasks = JSON.parse(localStorage.getItem("tasks"));
  if (storedTasks) {
    const taskToChange = storedTasks.incomplete.find(
      (task) => task == label.innerText
    );
    storedTasks.incomplete = storedTasks.incomplete.filter(
      (task) => task != taskToChange
    );

    storedTasks.completed.push(taskToChange);

    localStorage.setItem("tasks", JSON.stringify(storedTasks));
  }

  completedTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskIncomplete);
};

const taskIncomplete = function () {
  const listItem = this.parentNode;
  const label = listItem.querySelectorAll("label")[0];
  const taskToChange = storedTasks.completed.find(
    (task) => task == label.innerText
  );
  storedTasks.completed = storedTasks.completed.filter(
    (task) => task != label.innerText
  );

  storedTasks.incomplete.push(taskToChange);

  localStorage.setItem("tasks", JSON.stringify(storedTasks));

  incompleteTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskCompleted);
};

const bindTaskEvents = function (taskListItem, checkBoxEventHandler, cb) {
  const checkBox = taskListItem.querySelectorAll("input[type=checkbox]")[0];
  const editButton = taskListItem.querySelectorAll("button.edit")[0];
  const deleteButton = taskListItem.querySelectorAll("button.delete")[0];
  editButton.onclick = editTask;
  deleteButton.onclick = deleteTask;
  checkBox.onchange = checkBoxEventHandler;
};

addButton.addEventListener("click", addTask);

taskInput.addEventListener("keyup", (event) => {
  if (event.key === "Enter") {
    addButton.click();
  }
});

Array.from(incompleteTasksHolder.children).forEach((child) => {
  bindTaskEvents(child, taskCompleted);
});

Array.from(completedTasksHolder.children).forEach((child) => {
  bindTaskEvents(child, taskIncomplete);
});

if (storedTasks) {
  const completedTasks = storedTasks.completed || [];
  const incompleteTasks = storedTasks.incomplete || [];

  completedTasks.forEach((task) => {
    addTask(task, false, true);
  });
  incompleteTasks.forEach((task) => {
    addTask(task, false);
  });
}
